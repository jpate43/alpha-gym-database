<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Show Members on Day</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Showing Members on Specific Days.</h1>
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>

<div class="container ">
 <div class="panel-group">
    <div class="panel panel-default">
    <div class="panel-heading">Choose a Day</div>
    <div class="panel-body">
       <form role="form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <div class="form-group">
           <label for="dayName"  >Choose a day to view</label>
           <select name="dayName" class="form-control">
               <?php
                $result = mysqli_query($connection, "select dayID, dayName FROM DayOfWeek;");
                while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
                  echo "<option value=\"".$row['dayID']."\">".$row['dayName']."</option>";
                }
              ?>
            </select>
         </div>
         <button type="submit" class="btn btn-default">Submit</button>
       </form>
    </div>
  </div>
</div>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Member ID</th>
      <th>Name</th>
      <th>Class Name</th>
      <th>Day of the Week</th>
    </tr>
  </thead>
  <tbody>
<?php
    $query = "Select m.memberID as ID, concat(m.fName, ' ' , m.lName) as name, s.className as className, d.dayName as day
    FROM Member m
    JOIN Enroll e ON m.memberID = e.memberID
    JOIN Section s ON e.sectionID = s.sectionID
    JOIN DayOfWeekSectionJoin dj ON s.sectionID = dj.sectionID
    JOIN DayOfWeek d ON dj.dayID = d.dayID
    Where d.dayID = '".$_POST['dayName']."'
    order by m.memberID, m.fName, m.lName;";
    //echo $query;
    $result = mysqli_query($connection, $query);                                          // Returns the results of the query
    
    while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['ID']."</td>";
        echo "<td>".$row['name']."</td>";
        echo "<td>".$row['className']."</td>";
        echo "<td>".$row['day']."</td>";
        echo "</tr>";
    }
    
?>
</tbody>
</table>
  
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>