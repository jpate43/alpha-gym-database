<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Insert New Members</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Insert New Members into Database</h1>    
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>
</div>

<div class="container ">
 <div class="panel-group">
    <div class="panel panel-default">
    <div class="panel-heading">Add New Members</div>
    <div class="panel-body">
       <form role="form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <div class="form-group">
           <label for="fNameMem"  >Member's First Name</label>
           <input type="text" name="fNameMem" size="30" class="form-control"/>
         </div>
         <div class="form-group">
           <label for="lNameMem"  >Member's last Name</label>
           <input type="text" name="lNameMem" size="30" class="form-control"/>
         </div>
         <div class="form-group">
           <label for="addressMem"  > Member's Address</label>
           <input type="text" name="addressMem" size="30" class="form-control"/>
         </div>
         <div class="form-group">
           <label for="phoneNumber" >Member's Phone Number</label>
           <input type="number" name="phoneNumber" size="15" class="form-control"/>
         </div>
          <div class="form-group">
           <label for="staffIDMem"  >Member's Staff Trainer ID</label>
           <select name="staffIDMem" class="form-control">
               <?php
                $result = mysqli_query($connection, "select staffID, fName FROM Staff order by fName;");
                while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
                    echo "<option value=\"".$row['staffID']."\">".$row['fName']."</option>";
                }
                ?>
            </select>
         </div>
         <button type="submit" class="btn btn-default">Submit</button>
       </form>
    </div>
  </div>
</div>
</div>
<?php
        $memberCount;
        $query = "SELECT * FROM Member";
        $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
        while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
            $memberCount++;
        }
        $memberCount++;
        $staffCount;
        $query = "SELECT * FROM Staff";
        $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
        while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
            $staffCount++;
        }
        
        //INSERT INTO Member(memberID, fName, lName, address, phoneNumber, dateJoined ,staffID) VALUES (776,'Aaron','Fisher','41 DK Summit',1255059520,'2004-02-28',7);
        $fName = $_POST['fNameMem'];
        $lName = $_POST['lNameMem'];
        $address = $_POST['addressMem'];
        $phoneNum = $_POST['phoneNumber'];
        $date = $_POST['dateJoined'];
        $staffID = $_POST['staffIDMem'];
        $query = "INSERT INTO Member(memberID, fName, lName, address, phoneNumber, dateJoined ,staffID) VALUES (".$memberCount.",'".$fName."','".$lName."','".$address."',".$phoneNum.",'".date("Y-m-d")."',".$staffID.");";
        
        //echo $query;
        $result = mysqli_query($connection, $query); 
?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>