<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Update Time</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Update Time</h1>      
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>
</div>

<div class="container">
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading"> Update Class Time</div>
      <div class="panel-body">
        <form role="form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
          <div class="form-group">
            <label for="classTime"> Enter Minimum Class time</label> 
            <input type="number" name="classTime" min="10" class="form-control"/>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php
       
        
        //INSERT INTO Member(memberID, fName, lName, address, phoneNumber, dateJoined ,staffID) VALUES (776,'Aaron','Fisher','41 DK Summit',1255059520,'2004-02-28',7);
        $minTime = $_POST['classTime'];
        $query="UPDATE Section SET duration=".$minTime." WHERE duration <".$minTime.";";
        //echo $query;
        $result = mysqli_query($connection, $query); 
?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>