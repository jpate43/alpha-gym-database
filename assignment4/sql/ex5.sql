-- Select Members who are enrolled in two sections of the same class, show id, name and class name
select m.memberID, m.fName, m.lName, s.className
from Member m
JOIN Enroll e ON m.memberID = e.memberID
JOIN Section s ON e.sectionID = s.sectionID
Group BY m.memberID, m.fName, m.lName, s.className
HAVING count(s.className) > 1
Order BY m.memberID, m.fName, m.lName;

-- Select # of members for each staff
Select s.staffID, s.fName, s.lName, Count(m.memberID) as MemberCount
FROM Staff s
Left Join Member m
ON s.staffID = m.StaffID
Group By s.staffID, s.fName, s.lName
Order BY s.staffID, s.fName, s.lName;

-- Show supervisor names and all staff they supervise
Select Staff.fName,s.supervisorStaffID,s.fName,s.lName
From Staff
JOIN Staff s ON s.supervisorStaffID=Staff.staffID
where s.supervisorStaffID>0;


-- Show the classes a Member has to take on Tuesday
Select m.memberID, m.fName, m.lName, s.className, d.dayName
FROM Member m
JOIN Enroll e ON m.memberID = e.memberID
JOIN Section s ON e.sectionID = s.sectionID
JOIN DayOfWeekSectionJoin dj ON s.sectionID = dj.sectionID
JOIN DayOfWeek d ON dj.dayID = d.dayID
Group by m.memberID, m.fName, m.lName, s.className, d.dayName
Having d.dayName = 'Tuesday'
order by m.memberID, m.fName, m.lName;
-- show the service and staff in charged of that service on Monday per supervisor
select dwm.staffID, dwm.serviceName, concat(s.fName,' ',s.lName) as name, sup.fName as supervisor
from DayOfWeekMaintainJoin dwm
Join Staff s On dwm.staffID= s.staffID
JOIN Staff sup ON s.supervisorStaffID=sup.staffID
where dwm.dayID=1;

