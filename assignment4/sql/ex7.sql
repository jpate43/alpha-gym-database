
-- Show view of Members who are enrolled in two 
-- sections of the same class, show id, name and class name
create view serviceStaffMonday
as select dwm.staffID, dwm.serviceName, concat(s.fName,' ',s.lName) as name, sup.fName as supervisor
from DayOfWeekMaintainJoin dwm
Join Staff s On dwm.staffID= s.staffID
JOIN Staff sup ON s.supervisorStaffID=sup.staffID
where dwm.dayID=1;

select *from serviceStaffMonday; 

-- Show the view of classes a Member has to take on Tuesday
create view classMemTues
as Select m.memberID, m.fName, m.lName, s.className, d.dayName
FROM Member m
JOIN Enroll e ON m.memberID = e.memberID
JOIN Section s ON e.sectionID = s.sectionID
JOIN DayOfWeekSectionJoin dj ON s.sectionID = dj.sectionID
JOIN DayOfWeek d ON dj.dayID = d.dayID
Group by m.memberID, m.fName, m.lName, s.className, d.dayName
Having d.dayName = 'Tuesday'
order by m.memberID, m.fName, m.lName;

select *from classMemTues;

select name, staffID
from servicestaffmonday
where staffID!=1;

select concat(fname, ' ' ,lname) as name, className
from classmemtues
where memberID <50;
/*
This view is not updatable because it has a group by and having clause.
insert into classmemtues
values (2015,'Micheal','Jackson','Bond 007','Tuesday'); 

This view is not updatable because in a select statement in the view you
are not allowed to have an expression which concat() is. 
insert into servicestaffmonday
values (69,'happy ending','george clooney','Victor');
*/




