update Section
set duration=45
where duration<45;

Delete from Class
where className="";

delimiter |
create trigger enrollSectionRemove
before delete on Class FOR EACH ROW
Begin
delete from Enroll 
where sectionID IN
(Select sectionID 
	from Section
    where className=old.className);
delete from Section where className=old.className
order by sectionID;
End;
|
delimiter ;

-- Update the staffID and information for a staff
Update Staff s, Member m, Section sc
Set s.staffID=11,m.staffID=11,sc.staffID=11,s.fName='Oberyn',s.lName='Martel',s.address='1 Dorne', s.dateStarted='2015/02/12'
Where StaffID=8;
