create table IF not exists Member (
memberID INT unsigned Not null,
fName varchar(30) not null default '',
lName varchar (30) not null default '',
address varchar (30) not null default '',
phoneNumber bigint unsigned not null,
dateJoined date not null,
memberType varchar(30) not null default '',
billingInfo varchar(30) not null default '',
workoutProgram varchar(30) not null default '',
primary key (memberID)
);

create table IF not exists Staff (
staffID int unsigned not null,
supervisorStaffID int unsigned not null,
fName varchar(30) not null default'',
lName varchar(30) not null default '',
address varchar (30)  not null default '',
dateStarted date not null,

primary key (staffID)
);
alter table Member 
	add column staffID int unsigned not null;
alter table Member
		add foreign key (staffID) references Staff (staffID);

create table IF not exists Service ( 
serviceName varchar(30) not null default '',
hoursOfOperation int unsigned not null,
capacity int unsigned not null,
information varChar(30) not null default '',
primary key(serviceName)

);

create table IF not exists Class
(
className varchar(30) not null default '',
information varchar(30) not null default '',
primary key (className)
);

create table IF not exists Section
(
sectionID int unsigned not null,
classNumber int unsigned not null,
timeOffered int unsigned not null,
className varchar(30) not null default '',
staffID int unsigned not null,
duration int unsigned not null,
primary key (sectionID),
foreign key (className) references Class (className),
foreign key (staffID) references Staff (staffID)
);


create table if not exists Enroll
(
	memberID int unsigned not null,
    sectionID int unsigned not null,
    primary key (memberID, sectionID),
    foreign key (memberID) references Member (memberID),
    foreign key (sectionID) references Section (sectionID)
);


create table IF not exists DayOfWeek
(
	dayID int unsigned not null,
     dayName varchar(15) not null default '',
    primary key (dayID)
    
);

create table IF not exists DayOfWeekSectionJoin
(
	sectionID int unsigned not null,
    dayID int unsigned not null,
    primary key (dayID, sectionID),
    foreign key (sectionID) references Section (sectionID),
    foreign key (dayID) references DayOfWeek (dayID)
);

create table IF not exists Maintain
(
	staffID int unsigned not null,
    serviceName varchar (30) not null default '',
    primary key (staffID, serviceName),
    foreign key (staffID) references Staff (staffID)
    
);
create table IF not exists DayOfWeekMaintainJoin
(
	staffID int unsigned not null,
    dayID int unsigned not null,
    primary key (staffID,dayID),
    foreign key (staffID) references Staff(staffID),
    foreign Key (dayID) references DayOfWeek (dayID)

);



ALTER TABLE Member DROP COLUMN memberType;
alter table Member Drop column billingInfo;
alter Table Member Drop column workoutProgram;

select *From Member;

Alter table Section drop column timeOffered ;




