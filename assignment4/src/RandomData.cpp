#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include "names.h"

using namespace std;

static const char alphanum[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

int stringLength = sizeof(alphanum) - 1;

char genRandom()
{
	return alphanum[rand() % stringLength];
}



int main()
{
	srand(time(0));

	/*CREATE CLASSES*/

	ofstream classFile;
	classFile.open("class.txt");
	cout << "Creating random classes\n";
	std::vector<std::string> classlist;

	classlist.push_back("Zumba 101");
	classlist.push_back("Powerlifting 101");
	classlist.push_back("Crossfit 101");
	classlist.push_back("Spin 201");
	classlist.push_back("Walking 101");
	classlist.push_back("Speed Walking 301");
	classlist.push_back("Poor Form 101");
	classlist.push_back("Abs 101");
	classlist.push_back("Bond 007");
	classlist.push_back("Cardio 202");
	classlist.push_back("Kickboxing 101");
	classlist.push_back("Water Fitness 101");
	classlist.push_back("Circuit Training 101");
	classlist.push_back("Extreme Ironing 101");
	classlist.push_back("Parkour 101");

	/*CREATE SECTIONS*/

	ofstream sectionFile;
	sectionFile.open("section.txt");
	cout << "Creating random section\n";

	int sectionid(0), duration, staffID(1), classNum;

	for (unsigned int i = 0; i < classlist.size(); i++)
	{
		string current = classlist.at(i);
		string info = name::info[i];
		//Insert Statement for Class
		//classFile << current << "," << info << "\n";
		classFile << "INSERT INTO Class (className,information) VALUES ('" << current << "','" << info << "');\n";
		duration = (rand() % 6 + 3) * 10;

		for (unsigned int j = 0; j < 10; j++)
		{
			sectionid++;
			classNum = j + 1;
			staffID = (10 - j);
			//Insert Statements for Sections
			//sectionFile << sectionid << "," << classNum << "," << duration << "," << current << "," << staffID << "\n";
			sectionFile << "INSERT INTO Section(sectionID, classNumber, duration, className, staffID) VALUES (" << sectionid << "," << classNum << "," << duration << ",'" << current << "'," << staffID << ");\n";
		}
	}

	classFile.close();

	sectionFile.close();

	/*CREATE MEMBERS*/

	ofstream myfile;
	myfile.open("member.txt");
	cout << "Creating random members\n";
	int memberID(10), day,month,year;
	long phoneNum;
	string fname, lname, address;

	for (unsigned int i = 10; i < 1000; i++)
	{
		memberID++;
		phoneNum = rand() % 9999999 + 5550000000;
		staffID = rand() % 10 + 1;
		day = rand() % 28 + 1;
		month = rand() % 9 + 1;
		year = rand() % 20 + 1994;
		fname = name::firstName[rand() % 190];
		lname = name::lastNames[rand() % 100];
		int temp = rand() % 150 + 1;
		address += to_string(temp) + " " + name::roadNames[rand() % 33];
		//Insert Statements for members
		//myfile << memberID << "," << fname << "," << lname << "," << address << "," << phoneNum << ",'" << year << "-0" << month << "-" << day << "'," << staffID << "\n";
		myfile << "INSERT INTO Member(memberID, fName, lName, address, phoneNumber, dateJoined ,staffID) VALUES (" << memberID << ",'" << fname << "','" << lname << "','" << address << "'," << phoneNum << ",'" << year << "-0" << month << "-" << day << "'," << staffID << ");\n";
		fname.clear();
		lname.clear();
		address.clear();
	}

	myfile.close();

	/*ofstream myfile2;
	myfile2.open("staff.csv");
	cout << "Creating random staff\n";
	int supervisorID(0);
	staffID = 10;
	for (unsigned int i = 10; i < 1000; i++)
	{
		staffID++;
		if(i%5 == 0)
			supervisorID++;
		day = rand() % 28 + 1;
		month = rand() % 9 + 1;
		year = rand() % 30 + 1974;
		//phoneNum = rand() % 1000;
		fname = name::firstName[rand() % 190];
		lname = name::lastNames[rand() % 100];
		for (unsigned int i = 0; i < 4; ++i)
		{
			address += genRandom();
		}
		myfile2 << staffID << "," << supervisorID << "," << fname << "," << lname << "," << address << ",'" << year << "-0" << month << "-" << day << "'\n";
		fname.clear();
		lname.clear();
		address.clear();
		memberType.clear();
		billingInfo.clear();
		workoutProgram.clear();
	}

	myfile2.close();

	ofstream myfile3;
	myfile3.open("class.csv");
	cout << "Creating class sections\n";
	string className, classInfo;
	for (unsigned int i = 5; i < 100; i++)
	{
		className = name::businessName[i];
		classInfo = name::firstName[rand() % 190] + " " + name::businessName[rand() % 100] + " " + name::lastNames[rand() % 100];
		myfile3 << className << "," << classInfo << "\n";
	}
	myfile3.close();*/

	return 0;
}