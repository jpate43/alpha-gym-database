<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Section Tables</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Listed are all of the classes offered and sections available.</h1>
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>

  <h4>Classes</h4>
  <table class="table table-striped">
  <thead>
    <tr>
      <th>Class Name</th>
      <th>Information</th>
    </tr>
  </thead>
  <tbody>
  <?php
      $query = "SELECT * FROM Class";
      $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
      while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['className']."</td>";
        echo "<td>".$row['information']."</td>";
        echo "</tr>";
      }
  
  ?>
  </tbody>
  </table>
  
  <h4>Sections</h4>

  <table class="table table-striped">
  <thead>
    <tr>
      <th>Section ID</th>
      <th>Class Number</th>
      <th>Class Name</th>
      <th>Staff ID</th>
      <th>Duration</th>
    </tr>
  </thead>
  <tbody>
  <?php
      $query = "SELECT * FROM Section";
      $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
      while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['sectionID']."</td>";
        echo "<td>".$row['classNumber']."</td>";
        echo "<td>".$row['className']."</td>";
        echo "<td>".$row['staffID']."</td>";
        echo "<td>".$row['duration']."</td>";
        echo "</tr>";
      }
  
  ?>
  </tbody>
  </table>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>