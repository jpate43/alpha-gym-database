<!DOCTYPE html>
<html lang="en">
<head>
    
<title>AlphaGym</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Welcome to AlphaGym</h1>      
  </div>
  <p>This Website will connect to our gym database and allow you to perform queries and modifications on our data.</p>
     
</div>

<!-- Try out multiple buttons for functions -->

<div class="container">
  <h3>Select a function to perform.</h3>
  </br>
  <h5>View Tables</h5>
  <div class="btn-group btn-group-lg">
    <a href="staff.php" class="btn btn-primary" role="button">Staff</a>
    <a href="member.php" class="btn btn-primary" role="button">Member</a>
    <a href="section.php" class="btn btn-primary" role="button">Classes and Sections</a>
    <a href="services.php" class="btn btn-primary" role="button">Services</a>
  </div>
  </br>
  <h5>Run Queries</h5>
  <div class="btn-group btn-group-lg">
    <a href="insertMem.php" class="btn btn-primary" role="button">Insert Member</a>
   <a href="insertStaff.php" class="btn btn-primary" role="button">Insert Staff</a>
   <a href="showMemberDay.php" class="btn btn-primary" role="button">Find Members with classes on a Day</a>
    <a href="deleteClass.php" class="btn btn-primary" role="button">Delete Class</a>
    <a href="updateTime.php" class="btn btn-primary" role="button">Update Time</a>
  </div>
  </br>
    
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


</body>
</html>