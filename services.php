<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Services Tables</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Listed are all of the services offered by the gym.</h1>
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>

  <table class="table table-striped">
  <thead>
    <tr>
      <th>Service Name</th>
      <th>Hours of Operation</th>
      <th>Capacity</th>
      <th>Information</th>
    </tr>
  </thead>
  <tbody>
  <?php
      $query = "SELECT * FROM Service";
      $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
      while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['serviceName']."</td>";
        echo "<td>".$row['hoursOfOperation']."</td>";
        echo "<td>".$row['capacity']."</td>";
        echo "<td>".$row['information']."</td>";
        echo "</tr>";
      }
  
  ?>
  </tbody>
  </table>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>