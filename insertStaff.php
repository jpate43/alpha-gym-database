<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Insert New Staff</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Insert New Staff into Database</h1>    
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>
</div>

<div class="container ">
 <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">Add New Staff</div>
    <div class="panel-body">
       <form role="form"method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
         <div class="form-group">
           <label for="supervisor"  >Supervisor</label>
           <select name="supervisor" class="form-control">
               <?php
                $result = mysqli_query($connection, "select staffID, fName FROM Staff order by fName;");
                while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
                    echo "<option value=\"".$row['staffID']."\">".$row['fName']."</option>";
                }
                ?>
            </select>
         </div>
         <div class="form-group">
           <label for="fName"  >Staff's First Name</label>
           <input type="text" name="fName" size="30" class="form-control"/>
         </div>
         <div class="form-group">
           <label for="lName"  >Staff's last Name</label>
           <input type="text" name="lName" size="30" class="form-control"/>
         </div>
         <div class="form-group">
           <label for="address"  > Staff's Address</label>
           <input type="text" name="address" size="30" class="form-control"/>
         </div>
         <button type="submit" class="btn btn-default">Submit</button>
       </form>
    </div>
    
 </div>
</div>
</div>
<?php
        $staffCount;
        $query = "SELECT * FROM Staff";
        $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
        while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
            $staffCount++;
        }
        $staffCount++;
        
        $fName = $_POST['fName'];
        $lName = $_POST['lName'];
        $address = $_POST['address'];
        $supervisorStaffID = $_POST['supervisor'];
        $query = "INSERT INTO Staff(staffID,supervisorStaffID, fName, lName, address, dateStarted) VALUES (".$staffCount.",".$supervisorStaffID.",'".$fName."','".$lName."','".$address."','".date("Y-m-d")."');";

        $result = mysqli_query($connection, $query);
?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>