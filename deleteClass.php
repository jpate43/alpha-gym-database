<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Delete Class</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Delete Class </h1>      
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>
</div>

<div class="container">
  <div class="panel-group">
    <div class="panel panel-default">
      <div class="panel-heading"> Delete a Class</div>
      <div class="panel-body">
        <form role="form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
          <div class="form-group">
            <label for="className" class="whiteText"> Enter Class Name</label> 
            <input type="text" name="className" size="30" class="form-control"/>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
        </form>
      </div>
    </div>
    
</div>
</div>

<?php
  $class_name= $_POST['className'];
  $query="DELETE FROM Class WHERE className='".$class_name."';";
  //echo $query;
  $result = mysqli_query($connection, $query); 


?>

  <div class="container">
  <h4>Classes</h4>
  <table class="table table-striped">
  <thead>
    <tr>
      <th>Class Name</th>
      <th>Information</th>
    </tr>
  </thead>
  <tbody>
  <?php
      $query = "SELECT * FROM Class";
      $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
      while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['className']."</td>";
        echo "<td>".$row['information']."</td>";
        echo "</tr>";
      }
  
  ?>
  </tbody>
  </table>
  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>