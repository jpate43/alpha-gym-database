<!DOCTYPE html>
<html lang="en">
<head>
<?php require'connect.php'; ?>

<title>Member Tables</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap stylesheet -->
<link href="strapon.css" rel="stylesheet">
<!-- Include the stylesheet -->
<link rel="stylesheet" href="stylesheet.css">

</head>
<body>
    
<div class="container">
  <div class="page-header">
    <h1>Listed are all of the members in the gym.</h1>
    <a href="index.php" class="btn btn-primary" role="button">Click to return to Index</a>
  </div>

  <table class="table table-striped">
  <thead>
    <tr>
      <th>Member ID</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Address</th>
      <th>Phone Number</th>
      <th>Date Joined</th>
      <th>Staff ID</th>
    </tr>
  </thead>
  <tbody>
  <?php
      $query = "SELECT * FROM Member";
      $result = mysqli_query($connection, $query);                                          // Returns the results of the query
  
      while ($row = mysqli_fetch_assoc($result)) {                                          // Loop through each row of the results
        echo "<tr>";
        echo "<td>".$row['memberID']."</td>";
        echo "<td>".$row['fName']."</td>";
        echo "<td>".$row['lName']."</td>";
        echo "<td>".$row['address']."</td>";
        echo "<td>".$row['phoneNumber']."</td>";
        echo "<td>".$row['dateJoined']."</td>";
        echo "<td>".$row['staffID']."</td>";
        echo "</tr>";
      }
  
  ?>
  </tbody>
  </table>

</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    
</body>
</html>